<?php


namespace console\controllers;

use common\helpers\ApiHelper;
use common\models\Cities;
use yii\console\Controller;

/**
 * the console class that performs data import from certain web services
 *
 * Class ForecastController
 * @package console\controllers
 */
class ForecastController extends Controller
{
    /**
     *information action for using the options in the controller
     */
    public function actionHelp()
    {
        echo "Action update :\n";
        echo "  Parmeters:\n";
        echo "      start - date format d.m.Y required\n";
        echo "      end - date format d.m.Y required\n";
        echo "      list - City list format city1,city2,city3,...,cityN optional\n";
    }

    /**
     * A workflow that receives multithreaded data
     * from the http://quiz.dev.travelinsides.com/forecast/api/getForecast service and
     * writes them to the forecast table without duplicates.
     * @param $start
     * @param $end
     * @param null $list
     * @return bool|string
     */
    public function actionUpdate($start, $end, $list = null)
    {
        if(strtotime($start)>=strtotime($end))
        {
            echo "Error $start >= $end";
            return false;
        }
        ini_set('memory_limit','1024M');
        $counter = 0;
        if (empty($list)) {
            $cities = Cities::find()->select(["id", "name"]);
        } else {
            $list_array = explode(",", $list);
            if(!is_array($list_array) || empty($list_array))
            {
                echo "Error $list is not in correct syntax";
                return false;
            }
            $cities = Cities::find()->where(["in", "name", $list_array]);
            if ($cities->count() < 1) {
                echo $list . 'not found in database';
                return false;
            }
        }
        $data=[];
        $city_count=$cities->count();
        echo "Please wait... job for $city_count cities\n";
        foreach ($cities->each(100) as $city) {
            $data+= ApiHelper::getForecastUpdates($start, $end, $city);
            if (is_array($data)&&!empty($data)) {
                $counter += ApiHelper::updateForecasts($city->id,$data);
            }
        }
        echo "New forecasts : $counter";
        return $counter;
    }
}
