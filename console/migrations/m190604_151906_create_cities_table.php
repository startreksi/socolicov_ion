<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cities}}`.
 */
class m190604_151906_create_cities_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema("countries")) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }
            $this->createTable('{{%cities}}', [
                'id' => $this->primaryKey(),
                "country_id" => $this->integer()->notNull(),
                'name' => $this->text()->notNull()
            ], $tableOptions);
            $this->createIndex('idx-unique-cities-country_id-name', "{{%cities}}", ["country_id", "name"],true);
            $this->addForeignKey("cities_country_id_fkey", '{{%cities}}', "country_id", "countries", "id");
        } else {
            echo "Table Countries does not exists, please check it";
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("cities_country_id_fkey", "{{%cities}}");
        $this->dropTable('{{%cities}}');
    }
}
