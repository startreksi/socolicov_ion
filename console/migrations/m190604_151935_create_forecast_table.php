<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%forecast}}`.
 */
class m190604_151935_create_forecast_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        if ($this->db->getTableSchema("cities")) {
            $tableOptions = null;
            if ($this->db->driverName === 'mysql') {
                // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
                $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
            }
            $this->createTable('{{%forecast}}', [
                'id' => $this->primaryKey(),
                "city_id" => $this->integer()->notNull(),
                'temperature' => $this->float()->notNull(),
                'when_created' => $this->text()->notNull(),
            ], $tableOptions);
            $this->createIndex('idx-unique-forecast-city_id-when_created', "{{%forecast}}", ["city_id", "when_created"],true);
            $this->addForeignKey("forecast_city_id_fkey", '{{%forecast}}', "city_id", "cities", "id");
        } else {
            echo "Table Countries does not exists, please check it";
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("forecast_city_id_fkey", "{{%forecast}}");
        $this->dropTable('{{%forecast}}');
    }
}
