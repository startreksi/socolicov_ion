<?php
/* @var $this yii\web\View */
/* @var $cities array */

/* @var $form yii\widgets\ActiveForm */

use common\models\Countries;
use kartik\datetime\DateTimePicker;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

?>
<?php $form = ActiveForm::begin(); ?>

<?= $form->field($model, 'start')->widget(DateTimePicker::classname(), [
    'options' => ['readonly' => true, 'placeholder' => 'Enter event time ...'],
    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
    'removeButton' => false,
    'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy', 'minView' => "month"]
]); ?>

<?= $form->field($model, 'end')->widget(DateTimePicker::classname(), [
    'options' => ['readonly' => true, 'placeholder' => 'Enter event time ...'],
    'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
    'removeButton' => false,
    'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy', 'minView' => "month"]
]); ?>
<?= Html::a(Yii::t('admin', 'View List of Inputs'), ['/cities/index'], ['class' => 'btn btn-info',"target"=>"_blank"]) ?>

<?= $form->field($model, 'list')->textInput(); ?>

<div class="form-group">
    <?= Html::submitButton(Yii::t('admin', 'Import Forecasts'), ['class' => 'btn btn-success']) ?>
</div>

<?php ActiveForm::end(); ?>

