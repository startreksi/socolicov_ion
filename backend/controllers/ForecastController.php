<?php

namespace backend\controllers;

use backend\models\ImportForm;
use common\helpers\ApiHelper;
use common\models\Cities;
use common\models\Countries;
use Yii;
use common\models\Forecast;
use common\models\ForecastSearch;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ForecastController implements the CRUD actions for Forecast model.
 */
class ForecastController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Forecast models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ForecastSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Forecast model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Forecast model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Forecast();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Forecast model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Forecast model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * UI importing of forecasts data by list of cities
     * @return string|\yii\web\Response
     * @throws \yii\db\Exception
     */
    public function actionImport()
    {
        ini_set('memory_limit', '1024M');
        $importForm = new ImportForm();
        if ($importForm->load(Yii::$app->request->post()) && $importForm->validate()) {
            $counter = 0;
            $list = $importForm->list;
            $list = explode(",", $list);
            $cities = Cities::find()->where(["in", "name", $list]);
            if ($cities->count() < 1) {
                Yii::$app->session->setFlash('success', "$list not found in database';");

            } else {
                $data = [];
                foreach ($cities->each(100) as $city) {
                    $data += ApiHelper::getForecastUpdates($importForm->start, $importForm->end, $city);
                    if (is_array($data) && !empty($data)) {
                        $counter += ApiHelper::updateForecasts($city->id, $data);
                    }
                }
                Yii::$app->session->setFlash('success', "New forecasts : $counter");
                return $this->redirect(['index']);
            }
        }
        return $this->render("import", [
            "model" => $importForm,
        ]);

    }

    /**
     * Finds the Forecast model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Forecast the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Forecast::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('admin', 'The requested page does not exist.'));
    }
}
