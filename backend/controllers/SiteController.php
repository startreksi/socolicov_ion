<?php

namespace backend\controllers;

use common\helpers\ImportHelper;
use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;

/**
 * Site controller
 */
class SiteController extends Controller
{


    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Import data for test. Cities and countries from json file /cosole/migrations/data/countries_cities.json
     */
    public function actionImportTest()
    {
        $path = Yii::getAlias("@console/migrations") . DIRECTORY_SEPARATOR . 'data' . DIRECTORY_SEPARATOR . 'countries_cities.json';
        $importResult = ImportHelper::importTestData($path);
        if ($importResult) {
            Yii::$app->session->setFlash("success", "Successful import");
        } else {
            Yii::$app->session->setFlash("success", "Something was wrong on import");
        }
        return $this->redirect(Url::to(["/countries/index"]));
    }

}
