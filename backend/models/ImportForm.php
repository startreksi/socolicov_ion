<?php


namespace backend\models;

use yii\base\Model;

/**
 * Class ImportForm for UI import functionality
 * @package backend\models
 */
class ImportForm extends Model
{
    /**
     * Start date for searching
     * @var false|string
     */
    public $start;
    /**
     * End date for searching
     * @var false|string
     */
    public $end;
    /**
     * @var string List of cities to import forecasts( city1,city2,...,cityN)
     */
    public $list;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['start', 'end'], 'string'],
            ['start', 'compare', 'compareAttribute' => 'end', 'operator' => '<', 'type' => 'date'],
            [['start', 'end', 'list'], 'required'],
        ];
    }

    /**
     * ImportForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        if (empty($this->start)) {
            $this->start = date("d.m.Y", strtotime("-1 day"));
        }
        if (empty($this->end)) {
            $this->end = date("d.m.Y");
        }
        parent::__construct($config);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            "list" => \Yii::t("admin", "List ex(city1,city2,...,cityN)")
        ];
    }
}