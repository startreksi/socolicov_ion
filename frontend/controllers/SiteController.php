<?php

namespace frontend\controllers;

use common\models\Cities;
use common\models\ForecastSearch;
use frontend\models\StatsForm;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays stats.
     *
     * @return mixed
     */
    public function actionStats()
    {
        $searchModel = new StatsForm();
        $dataProvider = $searchModel->getContext(Yii::$app->request->queryParams);
        return $this->render('stats', [
            "statsForm" => $searchModel,
            "dataProvider" => $dataProvider,
        ]);
    }

    /**
     * Displays history of a city.
     * @param $city
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionHistory($city)
    {
        $model = Cities::findOne(["name" => $city]);
        if (empty($model)) {
            throw new NotFoundHttpException(Yii::t('admin', 'The requested city does not exist.'));
        }
        $forecasts = new ForecastSearch();
        $forecasts->city_id = $model->id;
        $dataProvider = $forecasts->searchDaily();

        return $this->render('history', [
            "dataProvider" => $dataProvider
        ]);
    }
}
