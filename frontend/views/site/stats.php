<?php

/* @var $this yii\web\View
 * @var $statsForm \frontend\models\StatsForm
 * @var $dataProvider \yii\data\ArrayDataProvider
 */

use common\helpers\TemperatureHelper;
use yii\bootstrap\ButtonDropdown;
use yii\grid\GridView;
use yii\widgets\Pjax;

$this->title = Yii::t('client', 'Statistics');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-stats">
    <?php Pjax::begin([
        'id' => 'stats-pjax',
        'timeout' => false,
        'enablePushState' => false,
        'clientOptions' => ['method' => 'GET']
    ]); ?>
    <?= $this->render('_stats_search', [
        'statsForm' => $statsForm,
    ]) ?>
    <div class="panel panel-default">
        <div class="panel-body">
            <?= GridView::widget([
                'id' => 'grid-StatsForm',
                'layout' => "{summary}<div class='pull-right'>{pager}</div>\n{items}\n<div class='pull-right'>{pager}</div>",
                'pager' => [
                    'firstPageLabel' => 'First',
                    'lastPageLabel' => 'Last'
                ],
                'dataProvider' => $dataProvider,
                'columns' => [
                    'country',
                    [
                        "attribute" => "city.name",
                        "label" => Yii::t("client", "City")
                    ],
                    [
                        "attribute" => "max_temperature",
                        "value" => function ($model) {
                            $tempertature = TemperatureHelper::fahrenheit_to_celsius($model["max_temperature"]);
                            if ($tempertature > 0) {
                                return '+' . $tempertature . '&#8451;';
                            }
                            return $tempertature . '&#8451;';

                        },
                        "format" => "raw"
                    ],
                    [
                        "attribute" => "min_temperature",
                        "value" => function ($model) {
                            $tempertature = TemperatureHelper::fahrenheit_to_celsius($model["min_temperature"]);
                            if ($tempertature > 0) {
                                return '+' . $tempertature . '&#8451;';
                            }
                            return $tempertature . '&#8451;';
                        },
                        "format" => "raw"
                    ],
                    [
                        "attribute" => "avg_temperature",
                        "value" => function ($model) {
                            $tempertature = round(TemperatureHelper::fahrenheit_to_celsius($model["avg_temperature"]));
                            if ($tempertature > 0) {
                                return '+' . $tempertature . '&#8451;';
                            }
                            return $tempertature . '&#8451;';
                        },
                        "format" => "raw"
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{all}',
                        'buttons' => [
                            'all' => function ($url, $model, $key) {
                                return ButtonDropdown::widget([
                                    'encodeLabel' => false, // if you're going to use html on the button label
                                    'label' => \Yii::t('client', 'Action'),
                                    'dropdown' => [
                                        'encodeLabels' => false, // if you're going to use html on the items' labels
                                        'items' => [
                                            [
                                                'label' => \Yii::t('client', 'History'),
                                                'url' => ['history', 'city' => $model["city"]["name"]],
                                            ],
                                        ],
                                        'options' => [
                                            'class' => 'dropdown-menu-left', // right dropdown
                                        ],
                                    ],
                                    'options' => [
                                        'class' => 'btn-default',   // btn-success, btn-info, et cetera
                                    ],
                                    'split' => false,    // if you want a split button
                                ]);
                            },
                        ],
                    ],
                ],
            ]); ?>
        </div>
    </div>
    <?php PJax::end() ?>
</div>
