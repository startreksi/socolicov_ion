<?php
/* @var $this yii\web\View
 * @var $statsForm \frontend\models\StatsForm
 */

use kartik\datetime\DateTimePicker;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

?>
<div class="panel panel-default">
    <div class="panel-heading">
        <?= Yii::t("client", "Search") ?>
    </div>

    <?php
    $form = ActiveForm::begin([
        "id" => "form-StatsForm",
        "method" => "GET",
        'options' => [
            'data-pjax' => 1
        ],
    ]);
    ?>
    <div class="panel-body row">
        <div class="col-xs-2">
            <?= $form->field($statsForm, 'start')->widget(DateTimePicker::classname(), [
                'options' => ['readonly' => true, 'placeholder' => 'Enter event time ...'],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy', 'minView' => "month"]
            ]); ?>
        </div>
        <div class="col-xs-2">
            <?= $form->field($statsForm, 'end')->widget(DateTimePicker::classname(), [
                'options' => ['readonly' => true, 'placeholder' => 'Enter event time ...'],
                'type' => DateTimePicker::TYPE_COMPONENT_APPEND,
                'removeButton' => false,
                'pluginOptions' => ['autoclose' => true, 'format' => 'dd.mm.yyyy', 'minView' => "month"]
            ]); ?>
        </div>
        <div class="col-xs-2">
            <div class="form-group">
                <?= Html::submitButton('<span class="glyphicon glyphicon-search"></span> ' .
                    Yii::t("client", "Search"), [
                    'class' => 'btn btn-success',
                    'style' => 'margin-top: 25px'
                ]) ?>

            </div>
        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>

</div>