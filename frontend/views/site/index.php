<?php

/* @var $this yii\web\View */

$this->title = 'Forecast';
?>
<div class="site-index">
    <div class="panel panel-default">
        <div class="panel-heading">
            Задание
        </div>
        <div class="panel-body">
            <h3>Консольная команда</h3>
            <p>
                Неоходимо создать job который будет качать с
                <a href="http://ctbsoft.com/forecast/api/getForecast?start=20.07.2014&amp;end=21.07.2014&amp;city=Moscow">
                    http://ctbsoft.com/forecast/api/getForecast?start=20.07.2014&amp;end=21.07.2014&amp;city=Moscow
                </a> данные и записывать их в таблицу <code>forecast</code> без дубликатов, дубликатами считаются записи
                у которых ряды <code>forecast.city_id</code> и <code>forecast.when_created</code> равны.
            </p>
            <p>
                Список городов находится в таблице <code>cities</code>.
            </p>
            <p>
                Даты произвольные в формате <code>d.m.Y</code>, передаются job'u как параметры(start, end).
            </p>
            <p>
                Данные желательно получать многопоточно.
            </p>
            <p>
                Ответ от <code>getForecast</code>
                <code>
                </code></p><code>
                <xmp>
                    <rows>
                        <row>
                            <city>Название города</city>
                            <temperature>Температура в форенгейтах</temperature>
                            <ts>Дата</ts
                        </row>
                    </rows>
                </xmp>
            </code>
            <p></p>
            <p>XML необходимо парсить через <code>DOMDocument</code>, можно использовать xpath.</p>
            <p>Job должен возвращать кол-во сохраненных записей.</p>
            <h3>Страница статистики</h3>
            <p>
                Нужно повторить фунционал страницы
                <a href="http://ctbsoft.com/forecast/site/stats">http://ctbsoft.com/forecast/site/stats</a>
            </p>
            <p>
                Метод перевода грудусов фаренгейта в цельсии: <br>
            </p>
            <pre>public function convertFahrenheitToCelsius($value)
{
    return (($value - 32) * 5) / 9;
}
</pre>
            <p></p>
            <p>HTML-представление цельсия: <code></code></p><code>
                <xmp>&#8451;</xmp>
            </code>
            <p></p>
            <p><a href="https://github.com/Eonasdan/bootstrap-datetimepicker/">DateTimePicker</a></p>
            <p><a href="http://getbootstrap.com/">Twitter bootstrap</a></p>
            <h3>Станица истории</h3>
            <p>
                Нужно повторить фунционал страницы
                <a href="http://ctbsoft.com/forecast/site/history/Moscow">http://ctbsoft.com/forecast/site/history/Moscow</a>
            </p>
            <p>
                Даты выделенные жирным, форматированы форматом <code>long</code>, зависящим от локали.
            </p>
            <p>
                URL должен быть как в примере.
            </p>
            <h3>Прочее</h3>
            <p>
                Имя проекта: <b>имя_фамилия (по-английcки)</b>
            </p>

            <p>
                <a href="http://ctbsoft.com/forecast/api/getDb">База данных</a> <br>
                <img alt=""
                     src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAHECAIAAADdytOUAAAkLUlEQVR42u2da3MUx7nHbS42LtshBhyD7zE4ScVlx44DWl12pdVqdRdI1sWwK4gkLCF0ARYhQuBIlo0WnMJIZUqXVeXozTlvJFnvkirjoU4lBnwqr/IF8g3OB8jr88z07qh3ei69YqWdmf3/66mtnp7untHOT093z87T81RFZRgGc5U9ha8ABihhMEAJA5QwGKCEAUoYDFDCACUMBihhMEAJA5QwGKCEAUoYDFDCACUMBihhgBLfAgxQwmCAEgYopewa5HoVI5SdkItVpFB2QC5WkULZDrlYRQrlJ5CLVaRQtkEuVpFC2Qq5WEUK5QnIxQKUaS1KC9AAyi2B8rggom34L//SrXvur6ZpKnYc2mIVKZQtgog2gs/RqFiLW9Xc+9W3yrdf9Ta3eFxFCmWzIKItzonfNKSbC6GmpvElZWm8qcmmTEPP7VVl9XZPQ7PHVaRQNgki2mKc+E1DuqkQami4sqAsXGlosCnT2NgYCoXos8njKlIoGwURbUlO/KYhLdYlF7WmKA80KcoSoZPOv7L0ICPKTWdSLleGz9ESygNOa7d79DLz2YcwFKbMsfoxvmX+rPSj25yte1SkUDYIItpOcuI3DWlDxfozdI1To8FguabKysp0PhGiLI5WVFBmRcXookaNml8/Nq/Mj9XXb7SQyaEEucMLoVCFpmCwa3pVWbt9Ri8zXFrKjsJyqDBrnxQMBvmW1aOvJbu0s6JdiUVqJn30lLL2ZVf6bMmzNrhPRQplvSCibZoTv2lI87Xq6k4n15T5kfJoNMpyamtrxXz6LB+ZJ0pO15E0dOrquEbSOSyRiETqNNXU1FR0frm6Om1T67J2OG2zjmuHjr72ZWcFOzpth0IXUqlEptZKMhZhtfTTdpUA5QaUkp4yG0rVXRFGhtbE/EgkwZNnAyW/Kxp1rmXazkL2MEDrqRdY4dLheXVz7dZpri6gLDyUdYLkPSVfq7b28pwyR+7K0JqYTznMsZnuYjmb22VVeDgQKM0WKxaJRMrKyoKjC+qY9VZ3nftUpFDWCiLaujjxm4Y0XysajU+trCTjUUNrWr6SSmzkRxMpZWUqTv2lUIXcITGUUHelEzntMiusHn0xEam1VlVVVaB9apXceTRa6zIVKZRRQfKe0lCRekNFBSBik099N/WnNE9huzq+WFGUVRrYsU3aq8JEHiyT0BvhdsUYZ+Iu05zSji9Wlazyoqpj0yvZLbhERQpljSCijX/4md80pA0VyeWUtJHL0W+yqCCm84fm9CHd3FAJ5bAqNEMvKRnih31z549Svk6V3jifExie4w9hXzgcDlN/fWGBv2e0wGotbJzq6lRbSY37VKRQRgTJP5Ah1iXIiIBjmmi4xucHAgHKpE9KG6qUl5eXaWJ3bVg+FTY0rueolJeU0CYdy7Ewqbq6mlpmVehTPzFKsFMVz8olKlIoqyEXC1BCgNIdUIYhF6tIoayCXKwihbIScrGKFMoQ5GIVKZRByMUqUihjkItVpFB2d1fDXGuAEgYoASUMUAJKQAkoYYAyH1DKPyIEYgDl9kE5ONiuG79pSPO14vGLywppcTIeNjRoswsGKLfQU8ZHp9dV8lZnRr0BZXzg0mzqzo2BMKD0AJTj4wO9vQ26Xbs21N9/wn+eUgbKeLw3mZpMFPS0AWX19ZFKZTb48F4Js+/vlt6ZPNnb2+gIpe2l9Wr3DShdAeX9uwGdSGbEKHnKRKKHjEBkCZa2J290ZjU71NplnjIDnJZILXGavdHGlUnvSnF0al42nZ9MhAUHvNGaYy3D0VPC/wCgrGYg6n23vplr9x2fXBTi/90MJYdOvO3GbIpxaeop44nJ1OylgUxmIrkxBlB3cUMCvrpVLUdnDChNPWVFrp4yHj81s65GCS5Pht07prSAMu3SNIBMdqnIZo1E1TLJXp1m3nFyh7CpBSidoPzjMI0pK4jFmWth+rx/t/TryVN9fY05eUqj1/QclA67svp6vc91ake2FqA0n30Tjt9cr6FP8pG0OTISy9FT+htKc4Zyakd+LgUoN6AkT7kyrbpM2uS948BA29mzLeQ7r14ddOy+12dO+Q5KYx9tsyu7+7aqBSg35Sl573j+fOfNm2OPHv3w44+PJyaG/DfRcYSJTaLNCVMnOvyciZvoWNQClHnwlOQjich/anr06O82t4TYbzyehpJxZnqzJpF0vu8jUwtQ5gDl0s1WRiQZ/9Mi9drkIxmUjx//gN+mt/reO6BM2+wf1B916NP0a5qYGCYcHz78GyVAno0N3LiT0u77AEo8T1kov6gOQLNu+nB3ywEloMSja4ASBigBJaD0OZRTkIsFTwmDpwSUMEAJKAEloIQBSkAJKBFiCwOU7l2MAAYo4SkBJTwlPOXWPJmG5ykBJaD0LJR6/IMYLGYfOJYV/7B80XSvIaBso/z69MyyYvqYOv8QO4v7cQ2UUksVmIZVqE9bzl7qj5k3AijztuqaMS4nw6UeSqbzN5oJenxgoY26QrgPCyd3p6e0WaogkdzgUi2GuO/t8ZSGwDEGn9nibMZI3HQ+gzWrjEqzvpgbX8yFUNosOsDv1Rc7AJTbuj6l6aYhiMwqVFxMuzMwMqelCvgCVhGPgHKLPaVZGHhxQGlHWBpKrn8HlHnwlDKLEdgsmJF7OmtZIpfPvm0WHciUR/e9BZ5SdjGCPHlK83UN3DKmzGGpAkNhbg0tO44BpZSnlFyMIF9jSpevayC5VMGlWKu+vKA4T7dqBFDK/syIxQjwM6MbH13DYgSAEs9TAkpACQOUgBIGKAEloHQ9lJDLVXRQ+swiXRfJ8D0AShdZ261/kOF7AJQucpMnl/9NBmcJKF3kJhmUcJaA0l1uEs4SULrOTcJZAko3ukk4S0DpOjcJZwko3egm4SwBpevcJJwloHSjm4SzBJSuc5NwloDSLUYg4ksAlIASUMIAJaAElIASBigBJaCEAUpACSgBZe7X46mntqgwoPT7v7IZDTlBacWTaX5OhQEloISnBJS52OtvvLlr166dO3cePvIuyykLVh569bWdmihBm6ZXV9+kxLu//NWzz+55+umnX3jhhY9+d5Rl6tKL0SH27H2Fiu3evbu0Iphpqoo2A2UVNjyVhypfOXhwx44du3c/8/N3Dtt7SpnCgNK99uZbb7+0bz8BQReS6NQx3X/gAEFDtv/Ay3q+DZQv/+wVjaqqnx8+8uKLP7Eq/7NXDnYs/B+lX3v9jbffOczy3//Nh/v2H7B3ctopvayfEn9o+cKA0hv2zDPPHi0pFTKf0TMpQf7GEUrdmxKX5AityhO4rPs+Gijbs2cPFaY0OeNf/fo9eyjV86TKmVOyh9KqMKD08LDPkGkD2Sby9TElOVfG4nPPPcfotDkTq0NsujCg9KCn5DyN7ik1OtP0lFaEnhDKjz4++pO9ez/87ccHD73q+D8j4/zgKX01piwxGVOyMVmIBpd6/gsvvviOOhmqovLk5xyhpHlSSWm5FZRke/f+9KV9+z748CNHKDOnFGKnJDemNBYGlF6afbOJNj/7fvW113doogEf8Zr2bb87SpNr8pc00T7y7i8coXzn8BFqwVCMh/K99z/QRpbOows6h4OHDtGhd+3axU+oTaG0KgwoYc6/6Bz5xS/feOttfCeA0i1Qkj9+/vnnj3H9OwxQFhJKNjYgT6nn7xCE7wpQ4ikhQAkoYYASUAJKGKAElIASUMIAJaAElDBACSgBJaCEAUpACShhgBJQAkpACQOUgBJQAkoYoASUgBIGKAEloCxOa7zwZ3wJbofyGuRreRXKTsin8jCUHZBP5WEo2yGfysNQfgL5VB6Gsg3yqTwMZSvkU3kYyhOQT+UrKBelhQsPKLcEyuOCiLbhv/xLt+65v5qmqdhxyMXyMJQtgog2gs/RqFiLhJp7v/pW+far3uacdkFPLg9D2SyIaItz4jcN6WYJNfTcXlVWb/c0NDWNLylL401N4q5maAvkYSibBBFtMU78piHdJKHGxsZQKESfDQ1XFpSFKw0N4q4maAvkYSgbBRFtSU78piEt1iXnt6YoDzQpyhIhSCwuZRLzFrvEupRr02YjJCEPQ9kgiGg7yYnfNKQNFevPED2p0WCwXFNlZaWaWT82r8yP1dezxHBpKdvL71LTY0vKWrJLqxsMBhOLa7fP1LMyKWXty650m+RZGyA5eRjKekFE2zQnftOQ5mvV1Z1OrinzI+XRaJTl1NbWavkaeXV1eoKrou+iumtfdlawurQdCl1IpRKZMivJWIRV0RuHHOU3KCU9ZTaUYzRkTEQihtbkoKS66Q5al6IssMKlw/Pq5tqt01xdyM9Q1gmS95R8rdray3PK3OXaWkNrer5YwLBrOBAozRYrFolEysrKgqMLhObare46SE4ehrJWENHWxYnfNKT5WtFofGplJRmPGlqLRhMEXIL63UzCbBfVVRYTkVprVVVVBdqnVskZR6O1kIQ8DGVUkLynNFSkflZR0YrwmZGIRh65u0iMkSfuUut2fLGqZO0VVR2bXsmUhxzlYShrBBFt/APM/KYhbahIzqykjZyZfvtmUWMxTR4VCAzPWe0Kh8PUX19YUPgxJSuzsNHg6lRbSQ0kJw9DGREk/0CGWLeyspLYOqaJBoIsk9IsoVJbUkKbVMawi1RdXV1eXs4K0KdenRKswUAgQO1HIDl5GMpqyKcClBCgzB+UYcin8jCUVZBP5WEoKyGfysNQhiCfysNQBiGfysNQxiCfysNQdndXw3xpgBIGKAElDFDCACWghAHKjFk9EHT79tT/LPTxhqsOKLcPysHBdt30zf7+4wTiwuedzAxQxuMXlxXS4mQ8bGjQZhcMUG6hp4yPTq+r5K3OjPofyvjApdnUnRsDYUC5TVCOjw/09jbodu3aUH//CXjKXKGMx3uTqclEQf9kn0B5faRSmQ0+vFfC7Pu7pXcmT/b2NvJQ6p+yl7Aou29AmTco798N6EQyI0bJUyYSPRcvns7JU47OrGYHcfvIU2aA0xKpJU6zN9q4MuldKY5Ozcum85OJsOCAN1pzrGU4ekr4H/AJlAxEve/WNx09pQHK+OSisLKAX6Hk0Im33ZhNMS5NPWU8MZmavTSQyUwkN8YA6i5uSMBXt6rl6Ix97CkrcvWU8fipmXU1/nB5MuzPMaUFlGmXpgFksktFNmskqpZJ9uo0846TO4RNreKA8o/DNKasIBZnroXp8/7d0q8nT/X1NebkKY1es6igdNiV1dfrfa5TO7K1/Dz7Jhy/uV5Dn+QjaXNkJJajpwSUdrtkZkU27cjPpfwGJXnKlWnVZdImf/P8m//oWPqia3y48fZ4q2P3vT5zClBmd98pw+TGald2921Vq4g9JblJ3VNOXT7+9a2L//vjD2QTE0OY6FhDaQITm0SbE6ZOdPg5EzfRsagFT5n2lOQjCcd/anr06O82t4TYbzxFCyXjzPRmTSLpfN9HplbRQbl0s5URScb/zEi9tg7l48c/4PdlN99799Wja7N/UH/UoU/TP3ViYphwfPjwb5QAPVtkAzfupLT7PoASVii/qA5As276cHfLASUMj64BShigBJSA0gNQTkE+FTwlDJ4SUMIAJQxQAkoYoIQBSixGAAOUW7sYgcd/5fNnfDA8pZehzHElBSxG4I3FCNzg1bbNU2Ixgm2F8tML51rnHzSlHjNruXf//OdfPOFiBO6HcisOByjzBmXLve90IpkRozIhtuLT5llPoXPRESzu1iSCQgg9y4qmWL7I1xIfaJ9c3sj8/vv/XF7f6I7TvfP69KgFIrmupIDFCLYVSgai3nfrmzKe0hj/oGMkxOuYxoPbQ6k3aAqlMXN9+qtlLnJNOwH9fByhdAwwwmIEBfaUJ+Q8pSGCUcxPg8j40JyWI5QGSkxrWUYIcSAyJ6p7aHsoZVZSwGIE2zumHB08oY0pu27cpM/j974b+vyLXBcjsHNCcuHh8rvsw9a0HLbLZGa96aB1LEZQgNk34Xhq4jZ9bm4xAldAqTk8lcVJhwHl9kKJxQieAErylG1/+m9KSC9G4NB9s3w2gch0xJYdpQSUWZ7PFB1+smI4K5nu22YlBSxGUHhPKbMYgTg/0J2T1URHnOQaJi5W7oqfaDM6zdHJTLzs++48TnSwGEFhPKXVYgQil463hJjxhElCqTszvUELf5YuZt93b2IlBSxGUBgoe2/dY0R6dzEC/T6Rfd+NxQi88ehax3V16k2fHl2MgHelBnf7QJA7H7bAYgT+i+1P88ePJt0MJRYjwEO+eJ4SUMIAJaCEAUoYoASUMEAJA5SAEgYocf0ApUejGUXhqgPKAsd9i2lcdUAJTwmDp4SndPdDQEX06BqgBJRuhJI9as6M3zSkTR/MEUNsDXutniVT1qdnlhXzh2q5R269+HCkBU9SUeGmT7CrD7bNXuqPmTcCT1lt82AYt4JA1rPifLDsAws5ho37z1PaRIUnkhtcqsWKKsT2CT2laaS22VJSxpgHfhGL7DKZoES5tS68C6VNfDe/V48rx5hS1lNaR9VYLmshk/bfC3FzigrnC1gFl8FTSnhKs6BVQCkT6G0/DOX7d3jKdHpgoO3s2Za+vsarVwc3sehA7ums2HAfz75t4rsz5Yu7+7bxlOfPd968Ofbo0Q8//vjY7iX0efKU5lHYfhhT5hAVbijMLVdkx3EReUrykUSk40vo8zWmlInC9iqXclHhl2Kt+kpu4jzdqpHi+pmRem3ykXgJPX5mdNeja3gJPaDE85QwQAkDlIASBigBJaB0PZSQj+VJKIvKIl0XyfA9AEoXWdutf5DhewCULnKTJ5f/TQZnCShd5CYZlHCWgNJdbhLOElC6zk3CWQJKN7pJOEtA6To3CWcJKN3oJuEsAaXr3CScJaB0o5uEswSUrnOTcJaA0i1GIOJLAJSAElDCACWgBJSAEgYoASWghAFKQAkoASX3RT+Vw1edU2FAWRxfSoaJnOBgUFpVMc3PqTCghG0SSnhKQJkHe/2NN3ft2rVz587DR97lmXiKU2lFaPfu3aUVwUytKtoMlFXYeMryUOUrBw/u2LFj9+5nfv7OYXtPKVMYUBaLvfnW2y/t2094ERZEp033/drrb7z9zmGWfv83H+7bf8C++6bW9h94mTgmo4Rpm46FAWUx2jPPPHu0pFRmTHk0ULZnzx7ykZQ+9Oprv/r1e/ZQqi0T7KxuSak9lFaFASVGjQ4TnZd/9gpj8bnnnmN02kBpqJvTgQAlPGWpJCsffXz0J3v3fvjbjw8eetVx9i3j/OApAaXlmLLEYkxJs5+S0nK+/N69P31p374PPvzIEcrMMDFEtv/AAbkxpbEwoCze2fdOTYbZN9k7h4/QjJhH5L33P9BGls73KYnyg4cOPf300zS15yfUplBaFQaUMGc78otfvvHW2/iZEVC6xcqClc8///yx7A4dUALKgtkOTeQpDTkb2vUsviVAiaeEACUMUAJKQAkoYYASUAJKGKAElIASUMIAJaAElDBACSgBJaCEAUpACShhgBJQAkpACQOUgBJQAkp8D4ASUAJKmI01XvgzvoTCQ3kNgixUSCg7IUhQgaHsgCBBBYayHYIEFRjKTyBIUIGhbIMgQQWGshWCBBUYyhMQJMh1UC5KCxcPUG4JlMcFEW3Df/mXbt1zfzVNUzFDxZaWvj99qzzQpHz7p76WluOQN1VgKFsEEW0En6NRMUPF8SVlLRmr1BQOh1sgz6rAUDYLItrinPhNQ5qv1dQ0vqgsJiKRZleKTm9JWRpvamqGJFRgKJsEEW0xTvymIc3Xami4Mq/MX2loaHKl6PQWlAXXnp7bVGAoGwURbUlO/KYhzdfSoEwPKNUxpbJEBKj5V5b0TMrSCy+ZFu65vZbJ1wunq3C7Nspz7eiZpoX50+NLQqYqMJQNgoi2k5z4TUOar1VfP0aecri0tDwjNXOMmFkcraigzYqK0UXCYayeFSa/xfJJwWAwXXgt2RUMspzE4trtM/Xpxs8QZKlRbReJxqz6cUOhULqR0QVl7daZetJYSln7sitdmAqIp9cA2arAUNYLItqmOfGbhjRfq65OvepjdXVczunkmjI/Uh6NRmmTPstH5gm703V1rPDl2tpMyTqt8NqXnRWsMG2HQhdSqYTYDqk2U5FPh8MXCfSxdOMryViE5WcaNJ4eZCM3QinpKZ2gVN0hTX30nEgkwcpYFN7o0DP9bBoyQzs897fW+GFAus3S4Xl1c+3W6cwhAKWXoKwTJO8p+Vq1tZfnlDlyfvY5zEFaFR4OBEqzZdpOpko3edDVm5+WlZVpZYf1YpFIhDKpQyc012512zQCmarAUNYKItq6OPGbhjRfKxpN0FVPRKNcTnxqRUkluJxESlmZilOHalF4MRERz0fbtZKMR4V81e9eCIWiWjtim1VVVYH2qVXysmZHhGxUYCijguQ9JV+Lumb1qkcifCZ1o4p285IVoA6a5hqWhTu+WNVKi6fEt8MdMcY4tjmB6tj0ipZpKAzZq8BQ1ggi2viHkPlNQ5qvpTPBZ5KvKhma08d8c0MllGNVOBwOUx98YYEfIy6wMmo7beTz9Hs6iyw/EBheyLq1xPhLLGyUXJ1qK2HtB4bnDNUhKxUYyogg+QcyDBWPHTsmtlZZWRkIBGgXfVLavnB1dXV5eXlJSQntpU8aF/LtELLHNOn5jGOWyZenBMvkD6qSrbVMVSKQrQoMZTUECQKUEKDMhjIMQYIKDGUVBAkqMJSVECSowFCGIEhQgaEMQpCgAkMZgyBBBYayu7saBjMYoIQBSkAJA5QwQAkoYf6CUv4RIVw5QLl9UA4OtuvGbxrSfK14/OKyQlqcjIcNDdrsggHKLfSU8dHpdZW81ZlRQGln8YFLs6k7NwbCgDIHKMfHB3p7G3S7dm2ov/8EPOV2QhmP9yZTk4mCfl0ugvL6SKUyG3x4r4TZ93dL70ye7O1tdITS9isGlDmCCyj5U7l/N6ATyYwYJU+ZSPSQEYgswdL25I3OrGZHcAPKLOC0RGqJ0+yNNq5MeleKo1Pzsun8ZCIsOOCN1hxrGY6eEv4HXAQlA1Hvu/XNXLvv+OSisKwAoBSh5NCJt92YTTEuTT1lPDGZmr00kMlMJDfGAOoubkjAV7eq5eiMXe4pK3L1lPH4qZl1NZhweTKM7lsSyrRL0wAy2aUimzUSVcske3WaecfJHcKmlneg/OMwjSkriMWZa2H6vH+39OvJU319jTl5SqPXBJSSUDrsyurr9T7XqR3ZWm6ffROO31yvoU/ykbQ5MhLL0VMCyq2A0pyhnNqRn0u5EUrylCvTqsukTd47Dgy0nT3bQr7z6tVBx+57feYUoMwTlMY+2mZXdvdtVcvjnpL3jufPd968Ofbo0Q8//vh4YmIIE52tgdIEJjaJNidMnejwcyZuomNRy1eeknwkEflPTY8e/d3mlhD7jQdQbgJKxpnpzZpE0vm+j0wtT0K5dLOVEUnG/7RIvTb5SAbl48c/ADK/3nt33aNrs39Qf9ShT9PTnZgYJhwfPvwbJUCAC23gxp2Udt8Hz1PCCuUX1QFo1k0f7m45oITh0TVACQOUMEC5TVBOQZAgeEoYPCWghAFKGKAElDBACQOUWIwAhsUI2jcdzZjfhykRyAtPmQdPabNIwWag9P6SB1iMYDNQXr7c19NTr9vVq+cGBlrz4imfnBsfeEosRpAzlJ9eOHd87kFT6jGzlnv3z01OyoTYFnxVAj9134AyC8qWe9/pRDIjRj/77Lhj4Jj4tLnoHVmafxZ9InZyZn2jO073zuvToxbXwwdLHmAxgpyhZCDqfbe+KeMpjfEPyxdNu28DQJPLXIiZFtnDKspA6cVIICxGsH2e0hDBKD+m5EFkgLIlDCRGqJ5c8gCLEeQ+phwdZGPKT69NqETe++7cxOc9PQ2bXnXNGcr0hJoy2V67eboPosuxGMEmZ9+EY+z6FH2Sj6TNkZFTm1jgShZKzeGpLE46DCiLHsoiXoyAQUmesvWr/6IEbcotRiDffRt9IT9ZMVSX6b69teQBFiPIj6eUWYxAnHYwn2dghY0aM5OSrEk3n1OEEx0sRrB5T2m1GIHIpUUnm3ZvTJk5SjrTvu/2x5IHWIxg81D23fyaEUm21YsR6LeK7PtuWFEvRtBx/SYRSZ9bvRgB7zgNU5YHgvCwBRYj2J7/6TR//GgSUGIxAjzkCwOUMEAJKGGAEgYoASUMUMIAJaCEAUoYDHHfMEC5rXHfMI/+Sg5PCQOUW+ApvRjhikU1ACWgBJRPAKX+qLkYl2Mfo2P6RA//HK7+xKSx/Pr0zLLDM2zic+mmLQvPtxuje63bdw79EVu2OY2t58nkqV6r2HDTWPKi8JQmF0yIWOAjYh9YiI8ZN60u33ImKiPriXc+VEMeSvOWLU6jIE7OJjbctFhReErLSEVhAYysi83nb0TcGvlg8WU8ZI4tWy+RZRNd6QBlVidgfRrbD6VNlLd8SIb/x5Smrs5qORf7tL3TsnF1No48H1BankYhoLSM8hafBU55Dsr8eUpXQGnou30NpVWU98aaMD70lE4voc+aixhWVpFZVsAGArH7dmxZ7lh25yzTfW/bOFKi+5aK8vYklDae0ibu2zymWwzNFseUtmn5iY7VmNLKU1qds2ElNwfPanEahZnoWEZ5Z/GaDaX5ZMhLntIm7ts0ptsqNHtzUBqX33Bq2fnmgMU587Dat1yo2HMbJ2caG255C0kLMBdjzbz0M+N2voQeN7fxQIasbdtL6AEloMTPgDBACQOUgBIGKGGAElDCACUMUAJKGKCEAUpACQOUiGaEFXfcN36e8daXUBSeElACSnhKN1KVly8hLy9/8NgrSwAloPRDjI4xqE97GDv9drpMgN9IzPjEruEVyjLh4fYvXrZqbXNR2/Kh4vZB6N35fim5/NvATQMk1BeazF7qj5k34itPyb8VmaXTsduZNyebx1hlCMspPNyRS5s3j8tHbcuHitucf/cWvKsv14jvRJILhKBinnu18uajGTfgy3qnnQ6r6WsVcw3i5mtZXzPnV5fmFLUtESpuc/75fyl5rhHf+l79feLFMqbkvm7+fU15jpeVuagyL3mWCZDNNVQ8X+ef94hvvYDVS0V96yk3evDlRf3l3ZQ29J5egXIToeKFhtKOsDSUXP/uK09pFffND554f2nVc5m+udsxiFsOyty6702FpcuHt+f/peQ5RXxnynu/+95c3PfGixY115ie7ljw5PzmbtMxpdxFlXnzuFPweG6h4jbR31sw0cnhbeDGiO8Ml/Yc+yfu2zAHTy9ooc+vnfCSCeKW9zSObx53jNrONVTcJvo774Hhkm8DvxRr5VdrMS65ZtEI4r5h+JnRU3Hfjj+N4PXLgNJ1P/cBSkAJA5SAEgYoYbDtg3I4o/Pnz5/5/e+ro7U8lBBkqi2HkiVC4er2zq6+vr4nOV4eTT8xx8x8NZ73o8CeFEpmQ0NDLBGsqj55KnZeEyVokzI/6++PROsoEY5EqWJVJEppcq6fffYZq9XZ9eng4CA1Eot3h8IR/RCtbZ+cO3dOP5ZpMfqviMXjlHnu3OAn7R1WULa1d1ABrW6czoqqU1OV1ZHMaYdps6q6JtOm+V574vm9pqfa0NRy9uxZyhwYGDje2gbOtgpKusDtHZ26p+z69OTpM2focpJ1nz5Dm5T56clT7BqcUCEbZOmWE61sLzlaqkKXnJo6FYtRYf0Q/BW1KkaXnw6kH9GKG74MVWGn+klHJytQ39h0+szv+SqmeyWhtDpVwpS4pAT9W+qZsPyPKen/nh9TkmOrrkmnKUEIah6imfwTJehSaQydpjQBR/mUILcRzlQht0cuVj+E7rpsig0MnAtzR7TiJpx1VudUt11TS22SF2T/Ns3HT/BVTPcOZ8sKSqtT7e8fIN/POgrYlnff9t06XVrtAquXJ/MZ1i85I5vJ9GLrm5LF5Acb9I/BaKOBBDsZ3sS9kp7S6lRr6urj3afpz6fxDHOZsO2AkpxQWPCUzEe2ftLOOkFKUz+ud5dEp6nzMBzCqtimPSVZtK6hp7e3tr6Bhr9iLXGvJJRWp6objQf0c4BtOZT6CI/GggQfGzWy0SSNqIhLStMnwUodGdtFQzcCNKxdRZoPsY5ePIRVMXZEOhw7os2YkpXRx5TMenp6afhBlJj+OYa9klBanSol2NiGGqRvA5xtE5TUO9MIjPVclKARlT5EoyrUf7FejHdd7CqSd6HMvrNn9bGdeAjTYnQImkzQ4egy28y+aRebDlNh/azIGptbqE2rP9OwV372bXqqlKCRAJ0DzcHRfW/rLzreMnLhHZzjzGkvDFDm38ivk9OyGv/Z74UByvwbG2OcyAxtc9oLA5QwGKCEAUoYDFDCACUMBihhgBIGA5QwQAmDFdz+H2O00VdGqDPHAAAAAElFTkSuQmCC">
            </p>
            <p>
                Переменные в <code>camelCase</code>, за исключением, свойств, которые описывают поля таблиц в
                <code>ActiveRecord</code> и аргументов методов.
            </p>
            <p>
                Методы в <code>camelCase</code>.
            </p>
        </div>
    </div>
</div>
