<?php

/* @var $this yii\web\View */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use common\widgets\HistoryForecast;

$this->title = Yii::t('client', 'History of {city} ({country})', ['city' => 'Moscow', 'country' => 'Russia']);
$this->params['breadcrumbs'][] = ['label' => Yii::t("client", "Statistics"), 'url' => ["/site/stats"]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-history">
    <?= HistoryForecast::widget([
        'title' => $this->title,
        'dataProvider' => $dataProvider
    ]) ?>
</div>
