<?php


namespace frontend\models;

use common\models\Forecast;
use yii\base\Model;
use yii\data\ArrayDataProvider;

/**
 * Class StatsForm
 * @package frontend\models
 */
class StatsForm extends Model
{
    /**
     * Start date for searching
     * @var false|string
     */
    public $start;
    /**
     * End date for searching
     * @var false|string
     */
    public $end;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['start', 'end'], 'string'],
            [['start', 'end'], 'required'],
        ];
    }

    /**
     * rewrite constructor for init start and end property
     * StatsForm constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        if (empty($this->start)) {
            $this->start = date("d.m.Y", strtotime("-1 day"));
        }
        if (empty($this->end)) {
            $this->end = date("d.m.Y");
        }
        parent::__construct($config);
    }

    /**
     * Find Forecasts by params with includes city and country name and temperature values
     * @param array $params
     * @return ArrayDataProvider
     */
    public function getContext($params)
    {
        $this->load($params);
        $query = Forecast::find()->select([
            "forecast.city_id",
            "countries.name as country",
            "cities.name as city",
            "max(forecast.temperature) as max_temperature",
            "min(forecast.temperature) as min_temperature",
            "avg(forecast.temperature)as avg_temperature"
        ])->innerJoinWith([
            "city",
            "city.country"
        ]);

        $query->groupBy([
            "country",
            "city",
            "city_id"
        ]);
        $query->andFilterWhere(['between', '"when_created"::int8', strtotime($this->start), strtotime($this->end)]);

        $query->asArray();
        //die(var_dump($this->start,strtotime($this->start)));
        $dataProvider = new ArrayDataProvider([
            'allModels' => $query->all(),
            'sort' => [
                'defaultOrder' => ['country' => SORT_ASC, 'city.name' => SORT_ASC],
                'attributes' => ['country', 'city.name', 'max_temperature', 'min_temperature', 'avg_temperature'],
            ],
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);
        return $dataProvider;
    }
}
