<?php


namespace common\helpers;

use common\models\Cities;
use common\models\Forecast;
use DateInterval;
use DatePeriod;
use DateTime;
use Yii;
use \Exception;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\httpclient\Client;
use yii\httpclient\XmlParser;

/**
 * Class ApiHelper
 * Provide methods to call multi threading API and save forecasts of City by period
 * @package common\helpers
 */
class ApiHelper
{
    const BASE_URL = 'http://quiz.dev.travelinsides.com/forecast/api';
    const PERIOD = 6;
    const WEB_SERVICE = 'getForecast';

    /**
     * Create requests for every 6 month of param period for indicate city to web service and
     * collect data in array
     * @param string $start d.m.Y date
     * @param string $end d.m.Y date
     * @param Cities $city
     * @return array
     * @throws Exception
     */
    public static function getForecastUpdates($start, $end, $city)
    {
        $period = self::PERIOD;
        $days = new DatePeriod(
            DateTime::createFromFormat("d.m.Y", $start),
            new DateInterval('P' . $period . 'M'),
            DateTime::createFromFormat("d.m.Y", $end)
        );
        $requests = [];
        $client = new Client([
            "baseUrl" => self::BASE_URL,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        /*verify if city exist on web service*/
        $now = date("d.m.Y", strtotime("-1 day"));
        $next = date("d.m.Y", strtotime($now . "+ 1 day"));
        $requests[] = $client->get(static::WEB_SERVICE, [
            'start' => $now,
            "end" => $next,
            "city" => $city->name
        ])->setOptions([
            CURLOPT_CONNECTTIMEOUT => 200000, // connection timeout
            CURLOPT_TIMEOUT => 20000, // data receiving timeout
        ]);
        $data = static::callApi($requests);;
        /*end*/

        $requests = [];

        if (!empty($data)) {
            foreach ($days as $day) {
                $current = $day->format('d.m.Y');
                $nextDay = date("d.m.Y", strtotime($current . "+ $period month"));
                if (strtotime($nextDay) > strtotime($end)) {
                    $nextDay = $end;
                }
                $requests[] = $client->get('getForecast', [
                    'start' => $current,
                    "end" => $nextDay,
                    "city" => $city->name
                ])->setOptions([
                    CURLOPT_CONNECTTIMEOUT => 200000, // connection timeout
                    CURLOPT_TIMEOUT => 20000, // data receiving timeout
                ]);
            }
        }

        if (!empty($requests)) {
            $data = static::callApi($requests);
        }

        return $data;

    }

    /**
     * Multi-threading call of web service by CURL and parse data into array
     * @param $requests
     * @return array
     * @throws InvalidConfigException
     * @throws \yii\httpclient\Exception
     */
    protected static function callApi($requests)
    {
        $client = new Client([
            "baseUrl" => self::BASE_URL,
            'transport' => 'yii\httpclient\CurlTransport'
        ]);
        $forecastResponses = $client->batchSend($requests);
        $data = [];
        foreach ($forecastResponses as $response) {
            if (!empty($response)) {
                $parser = new XmlParser();
                $row = $parser->parse($response);
                if (empty($row["error"])) {
                    $data[] = $row["row"];
                }
            }
        }
        return $data;
    }


    /**
     * Batch insert of forecast data from dataF array and return number of rows are inserted
     * @param $city_id
     * @param $dataF
     * @return int
     * @throws \yii\db\Exception
     */
    public static function updateForecasts($city_id, $dataF)
    {
        $count = 0;
        foreach ($dataF as $data) {
            $formated = ArrayHelper::getColumn($data, function ($data) use ($city_id) {
                if (!empty($data['temperature']) && !empty($data["ts"]) && !empty($data["city"])) {
                    return [
                        'city_id' => $city_id,
                        'temperature' => $data['temperature'],
                        'when_created' => $data['ts'],
                    ];
                }
            });
            $command = Yii::$app->db->createCommand();
            $command->batchInsert(Forecast::tableName(), ["city_id", "temperature", "when_created"], $formated);
            $fullCommand = $command->sql . ' ON CONFLICT (city_id, when_created) DO NOTHING';
            $command->sql = $fullCommand;
            $count += $command->execute();
        }
        return $count;
    }
}
