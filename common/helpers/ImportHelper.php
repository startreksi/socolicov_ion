<?php


namespace common\helpers;


use common\models\Cities;
use common\models\Countries;

/**
 * Helper to import data (countries,cities) from json file
 * Class ImportHelper
 * @package common\helpers
 */
class ImportHelper
{
    /**
     * This function import test data from json cities and countries with $path and return this result
     * @param $path
     * @return bool
     */
    public static function importTestData($path)
    {
        $cityCount = 0;
        $countryCount = 0;
        $importData = json_decode(file_get_contents($path), true);
        array_filter($importData, function ($value, $key) use (&$cityCount, &$countryCount) {
            $country = Countries::findOne(["name" => $key]);
            if (empty($country)) {
                $country = new Countries();
                $country->name = $key;
                if ($country->save()) {
                    $countryCount++;
                }
            }
            if (empty($country->errors)) {
                if (count($value) > 40) {
                    $chunked = array_chunk($value, 100);
                } else {
                    $chunked = [$value];
                }

                foreach ($chunked as $cities) {
                    $cityCount += static::importCity($country, $cities);
                }
            } else {
                echo "Country $country " . " can't imported, errors:" . json_encode($country->errors);
            }
        }, ARRAY_FILTER_USE_BOTH);
        return true;
    }

    /**
     * Function to import cities for $country from array value of cities and return number of rows saved
     * @param Countries $country
     * @param array $value
     * @return int
     * @throws \yii\db\Exception
     */
    protected static function importCity($country, $value)
    {
        $command = \Yii::$app->db->createCommand();
        $optimized = [];
        array_filter($value, function ($city) use (&$optimized, $country) {
            $optimized[] = ["name" => $city, "country_id" => $country->id];
        });
        $command->batchInsert(Cities::tableName(), ["name", "country_id"], $optimized);
        $fullCommand = $command->sql . ' ON CONFLICT (name, country_id) DO NOTHING';
        $command->sql = $fullCommand;
        return $command->execute();
    }
}