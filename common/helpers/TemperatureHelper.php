<?php


namespace common\helpers;

/**
 * Helper contains methods for temperature converting
 * Class TemperatureHelper
 * @package common\helpers
 */
class TemperatureHelper
{
    /**
     * convert value fahrenheit to celsius
     * @param $given_value
     * @return float|int
     */
    public static function fahrenheit_to_celsius($given_value)
    {
        return (($given_value - 32) * 5) / 9;
    }
}