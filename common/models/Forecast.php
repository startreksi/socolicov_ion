<?php

namespace common\models;

use common\helpers\TemperatureHelper;
use Yii;

/**
 * This is the model class for table "forecast".
 *
 * @property int $id
 * @property int $city_id
 * @property double $temperature
 * @property string $when_created
 *
 * @property Cities $city
 */
class Forecast extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'forecast';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'temperature', 'when_created'], 'required'],
            [['city_id'], 'default', 'value' => null],
            [['city_id'], 'integer'],
            [['temperature'], 'number'],
            [['when_created'], 'string'],
            [['city_id', 'when_created'], 'unique', 'targetAttribute' => ['city_id', 'when_created']],
            [
                ['city_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cities::className(),
                'targetAttribute' => ['city_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City ID'),
            'temperature' => Yii::t('app', 'Temperature'),
            'temperatureCelsius' => Yii::t('app', 'Temperature Celsius'),
            'when_created' => Yii::t('app', 'When Created'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(Cities::className(), ['id' => 'city_id']);
    }

    /**
     * Convert temperature fahrenheit to celsius
     * @return float|int
     */
    public function getTemperatureCelsius()
    {
        return TemperatureHelper::fahrenheit_to_celsius($this->temperature);
    }

}