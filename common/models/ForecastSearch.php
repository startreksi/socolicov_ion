<?php

namespace common\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Forecast;

/**
 * ForecastSearch represents the model behind the search form of `common\models\Forecast`.
 */
class ForecastSearch extends Forecast
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'city_id'], 'integer'],
            [['temperature'], 'number'],
            [['when_created'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Forecast::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'temperature' => $this->temperature,
        ]);

        $query->andFilterWhere(['ilike', 'when_created', $this->when_created]);

        return $dataProvider;
    }

    /**
     * Creates data provider of Forecast objects with
     * when_created and temperature attributes instance with search query applied
     * city_id required
     * @return ActiveDataProvider|bool
     */
    public function searchDaily()
    {
        if (!empty($this->city)) {
            $query = Forecast::find()->select([
                '"when_created"',
                "temperature"
            ]);
            $query->orderBy(['"when_created"::int8' => SORT_DESC]);

            // add conditions that should always apply here

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);


            if (!$this->validate()) {
                // uncomment the following line if you do not want to return any records when validation fails
                // $query->where('0=1');
                return $dataProvider;
            }

            // grid filtering conditions
            $query->andFilterWhere([
                'city_id' => $this->city_id,
            ]);

            $query->andFilterWhere(['ilike', 'when_created', $this->when_created]);

            return $dataProvider;
        }
        return false;
    }
}
