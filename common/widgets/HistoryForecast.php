<?php

namespace common\widgets;

use common\helpers\TemperatureHelper;
use common\models\Forecast;
use Yii;
use yii\bootstrap\Widget;
use yii\data\ActiveDataProvider;
use yii\widgets\LinkPager;

/**
 *Widget to render City forecast story
 *
 * @author Socolicov Ion <isocolicov@gmail.com>
 */
class HistoryForecast extends Widget
{
    /**
     * @var array the alert types configuration for the flash messages.
     * title of panel
     */
    public $title;
    /**
     * @var ActiveDataProvider Forecast data Provider of Forecasts Model with columns
     * - when_created
     * - temperature
     */
    public $dataProvider;
    /**
     * Size of one page data
     * @var int
     */
    public $pageSize = 100;
    /**
     * @var Forecast[] array of forecast objects
     */
    protected $items;
    /**
     * @var string template of parent object( long date)
     */
    public $parentTemplate = <<<HTML
     <p>
        <strong>
            {parent}
        </strong>
     </p>
HTML;
    /**
     * @var string template of child object (temperature value in Celsius)
     */
    public $childTemplate = <<<HTML
    <p>
        {child}
    </p>
HTML;
    /**
     * @var string item template including parent and his childs
     */
    public $itemTemplate = <<<HTML
    <div class="col-xs-3">
        {item}
    </div>
HTML;

    /**
     * @var string general template of widget
     */
    public $generalTemplate = <<<HTML
 <div class="panel panel-default">
        <div class="panel-heading">
            {title}
        </div>
        <div class="panel-body">
            <div class="row">
            {pager}
            </div>
            <div class="row">
            {content}
            </div>
            <div class="row">
            {pager}
            </div>
        </div>
 </div>
HTML;

    /**
     * init function for set page size of entered data Provider
     */
    public function init()
    {
        if (!empty($this->dataProvider)) {
            $this->dataProvider->pagination->pageSize = $this->pageSize;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function run()
    {
        return $this->renderItems();
    }

    /**
     * render content of widget grouping days in items
     * @return string
     * @throws \Exception
     */
    public function renderItems()
    {
        $content = "";
        $itemContent = "";
        $day = null;
        foreach ($this->dataProvider->models as $item) {
            if (empty($day) || date("M d,Y", $item->when_created) <> $day) {
                if (!empty($day)) {
                    $content .= strtr($this->itemTemplate, ["{item}" => $itemContent]);
                }
                $day = date("M d,Y", $item->when_created);
                $itemContent = strtr($this->parentTemplate, ["{parent}" => $day]);
            }
            $temperature = TemperatureHelper::fahrenheit_to_celsius($item->temperature);
            $temperature = ($temperature > 0) ? '+ ' . $temperature . '&#8451;' : $temperature . '&#8451;';
            $itemContent .= strtr($this->childTemplate, [
                "{child}" => date("H:i:s", $item->when_created) . ' ' . $temperature
            ]);
        }
        $pager = LinkPager::widget([
            "pagination" => $this->dataProvider->pagination
        ]);
        return strtr($this->generalTemplate, [
            '{title}' => $this->title,
            '{content}' => $content,
            '{pager}' => $pager
        ]);
    }
}
