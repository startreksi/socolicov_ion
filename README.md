**Forecast Test for PHP DEVELOPER**

**Installation**

1. Clone application

2. Install nginx, pgsql, php 7.2 with pgsql package and php7.2 curl

3. copy /forecast.conf in nginx configuration and replace default_roots with your project root

4. php init

5. Create database and set main-local

` 'db' => [
            'class' => 'yii\db\Connection',
            
            'dsn' => 'pgsql:host=localhost;dbname=forecast',
            
            'username' => 'postgres',
            
            'password' => '123qwe',
            
            'charset' => 'utf8',
        ],
`

6. apply migrations php yii migrate/up

7. composer update

8. port 80 frontend, port 80/admin backend

**User Guide**

**To import first test data for cities and countries table**

go to localhost\admin and select Import Countries&&Vities on the top menu

**To import manual data for cities and countries table**

go to localhost\admin and select Cities or Countries on the top menu
and use default functional to add data.

**To import data from web service >**

`./yii forecast/update start end [list]`

Parameters

_start_ - d.m.Y start of period to import data from Web Service

_end_ - d.m.Y start of period to import data from Web Service

_list_ - optional paramter to indicate list of city to import forecast with "," delimitator
ex. ./yii forecast/update 20.07.2018 20.08.2018 Moscow,New York . This cities should be already in DataBase